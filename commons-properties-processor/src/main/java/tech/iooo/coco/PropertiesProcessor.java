package tech.iooo.coco;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;

/**
 * Created by Ivan97 on 2017/7/27 下午3:51
 */
public class PropertiesProcessor {

    private PropertiesProcessor(){}

    private static final Logger logger = LoggerFactory.getLogger(PropertiesProcessor.class);

    public static Properties loadProperties(String fileName) {
        Properties properties = new Properties();
        try {
            InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
            if (Objects.nonNull(is)) {
                properties.load(is);
                is.close();
            }
        } catch (IOException e) {
            logger.error("Error occured while loading property file [{}] :\n", fileName, e);
        }
        return properties;
    }

}
