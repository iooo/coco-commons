package tech.iooo.coco.fastdfs.common;

/**
 * Created by OsbornHu
 * Email:hujian@foreveross.com
 * Date: 2016/6/8.
 */
public class FileStoreException extends RuntimeException {
    private static final long serialVersionUID = 4385353255994952486L;

    public FileStoreException() {
        super();
        // TODO Auto-generated constructor stub
    }

    public FileStoreException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

    public FileStoreException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public FileStoreException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

}
