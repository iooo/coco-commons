package tech.iooo.coco.fastdfs.common;

/**
 * Created by OsbornHu
 * Email:hujian@foreveross.com
 * Date: 2016/6/8.
 */
public class FormatableCharSequence implements java.io.Serializable, CharSequence {

    private static final long serialVersionUID = 4667459143425384729L;
    private StringBuilder builder = new StringBuilder(64);
    private Object[] parameters = new Object[0];
    private String toString = null;

    protected FormatableCharSequence(CharSequence formatString,
                                     Object[] parameters) {
        String string = formatString == null ? "" : formatString.toString();
        builder.append(string);
        this.parameters = parameters == null ? this.parameters : parameters;
    }

    /**
     * return FormatableCharSequence instance
     *
     * @param formatString A CharSequence type can be FormatableCharSequence
     * @param params       Use to format the formatString
     * @return
     * @author <a href="mailto:iffiff1@hotmail.com">Tyler Chen</a>
     * @since 2014-3-14
     */
    public static FormatableCharSequence get(CharSequence formatString,
                                             Object... params) {
        FormatableCharSequence fs = new FormatableCharSequence(formatString, params);
        return fs;
    }

    @Override
    public char charAt(int index) {
        return builder.charAt(index);
    }

    @Override
    public int length() {
        return builder.length();
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return builder.subSequence(start, end);
    }


}

