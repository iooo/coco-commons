package tech.iooo.coco.fastdfs.common;

import org.apache.commons.lang.StringUtils;
import org.csource.common.MyException;
import org.csource.fastdfs.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

/**
 * Created by OsbornHu
 * Email:hujian@foreveross.com
 * Date: 2016/6/8.
 */
public class StorageClientFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(StorageClientFactory.class);
    private static TrackerGroup trackerGroup;
    private static boolean inited = false;

    private static void writeToFile(InputStream inputStream,String outputPath) throws Exception {
        File tempFile = new File(outputPath);
        if(tempFile.exists()){
            tempFile.delete();
        }
        try (OutputStream outputStream = new FileOutputStream(outputPath)) {

            int bytesWritten = 0;
            int byteCount = 0;

            byte[] bytes = new byte[1024];

            while ((byteCount = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, bytesWritten, byteCount);
                bytesWritten += byteCount;
            }
            inputStream.close();
            outputStream.close();
        }
    }

    public static void init(InputStream config) throws FileNotFoundException, IOException, MyException {
        if (inited) {
            return;
        }

        String configPath = "./config.properties";
        try {
            writeToFile(config,configPath);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ClientGlobal.init(configPath);
        inited = true;

        trackerGroup = ClientGlobal.getG_tracker_group();
    }

    public static StorageClient getStorageClient(String groupName) {
        if (trackerGroup == null) {
            throw new StorageUnreachableException("获取FastDFS的StorageClient失败");
        }
        TrackerClient trackerClient = new TrackerClient(trackerGroup);
        TrackerServer trackerServer = null;
        try {
            trackerServer = trackerClient.getConnection();
        } catch (IOException e) {
            LOGGER.error("无法连接到FastDFS的Tracker Server");
            throw new StorageUnreachableException("获取FastDFS的StorageClient失败");
        }

        StorageServer storageServer = null;
        try {
            if (StringUtils.isNotBlank(groupName)) {
                storageServer = trackerClient.getStoreStorage(trackerServer, groupName);
            } else {
                storageServer = trackerClient.getStoreStorage(trackerServer);
            }
        } catch (IOException e) {
            LOGGER.error("无法连接到FastDFS的Storage Server");
            throw new StorageUnreachableException("获取FastDFS的StorageClient失败");
        }

        if (storageServer == null) {
            LOGGER.error("无法连接到FastDFS的Storage Server");
            throw new StorageUnreachableException("获取FastDFS的StorageClient失败");
        }

        StorageClient storageClient = new StorageClient(trackerServer, storageServer);

        return storageClient;
    }

}