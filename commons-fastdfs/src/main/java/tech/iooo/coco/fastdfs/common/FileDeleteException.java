package tech.iooo.coco.fastdfs.common;

/**
 * Created by OsbornHu
 * Email:hujian@foreveross.com
 * Date: 2016/6/8.
 */
public class FileDeleteException extends RuntimeException {
    private static final long serialVersionUID = 1796824303307189534L;

    public FileDeleteException() {
        super();
        // TODO Auto-generated constructor stub
    }

    public FileDeleteException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

    public FileDeleteException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public FileDeleteException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

}
