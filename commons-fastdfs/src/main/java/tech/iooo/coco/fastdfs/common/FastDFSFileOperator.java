package tech.iooo.coco.fastdfs.common;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.csource.common.MyException;
import org.csource.fastdfs.StorageClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Random;

/**
 * Created by OsbornHu
 * Email:hujian@foreveross.com
 * Date: 2016/6/8.
 */
public class FastDFSFileOperator {
    private static final Logger logger = LoggerFactory.getLogger(FastDFSFileOperator.class);
    private static final String DEFAULT_CONFIG_PATH = "classpath:fastdfs-client.conf";
    private static final String DEFAULT_IMAGE_GROUPS = "coco";

    private String[] imageGroupNames;
    private int groups = -1;
    private InputStream configFile;

    public void setImageGroupNames(String imageGroupNames) {
        parseImageGroupNames(imageGroupNames);
    }

    public void setConfig(String config) {
        initStorageClientFactory(config);
    }

    private void getImageGroupNames() {
        if (imageGroupNames == null && groups == -1) {
            parseImageGroupNames(DEFAULT_IMAGE_GROUPS);
        }
    }

    private void parseImageGroupNames(String groupNames) {
        imageGroupNames = StringUtils.split(groupNames, ",");
        this.groups = imageGroupNames.length;
    }

    private void initStorageConfig() {
        if (configFile == null) {
            initStorageClientFactory(DEFAULT_CONFIG_PATH);
        }
    }

    private void initStorageClientFactory(String config) {
        configFile = ResourceKit.getFileResouce(config);
        try {
            StorageClientFactory.init(configFile);
        } catch (Exception ex) {
            configFile = null;
            logger.error("FastDfs初始化连接失败", ex);
        }

    }

    public String getRandomImageGroup() {
        getImageGroupNames();
        int num = (new Random()).nextInt(groups);
        return imageGroupNames[num];
    }

    public String getImageGroup(String remoteFilename) {
        String[] pair = parseFilename(remoteFilename);

        return pair[0];
    }

    public String[] parseFilename(String remoteFilename) {
        String[] pair = new String[2];

        if (remoteFilename.charAt(0) == '/') {
            remoteFilename = remoteFilename.substring(1);
        }

        int index = remoteFilename.indexOf("/");

        pair[0] = remoteFilename.substring(0, index);
        pair[1] = remoteFilename.substring(index + 1);

        return pair;
    }

    public String getRemouteFile(String remoteFilename) {
        String[] pair = parseFilename(remoteFilename);

        return pair[1];
    }

    /**
     * @param file
     * @return results[0]: the group name to store the file<br/>
     * results[1]: the new created filename
     * @throws StorageUnreachableException
     */
    public String[] uploadFile(File file) {
        String filePath = file.getPath();
        String ext = FilenameUtils.getExtension(filePath);

        return uploadFile(file, ext);
    }

    /**
     * @param file
     * @param ext
     * @return results[0]: the group name to store the file<br/>
     * results[1]: the new created filename
     * @throws StorageUnreachableException
     */
    public String[] uploadFile(File file, String ext) {
        if (Objects.isNull(file)) {
            throw new FileStoreException("file为null,无法上传");
        }

        initStorageConfig();

        StorageClient client = StorageClientFactory.getStorageClient(getRandomImageGroup());

        String filePath = file.getPath();

        try {
            return client.upload_file(filePath, ext, null);
        } catch (IOException e) {
            throw new FileStoreException(e);
        } catch (MyException e) {
            throw new FileStoreException(e);
        }
    }

    /**
     * @param file
     * @param ext
     * @return results[0]: the group name to store the file<br/>
     * results[1]: the new created filename
     * @throws StorageUnreachableException
     */
    public String[] uploadFile(byte[] file, String ext) {
        initStorageConfig();

        StorageClient client = StorageClientFactory.getStorageClient(getRandomImageGroup());

        try {
            return client.upload_file(file, ext, null);
        } catch (IOException e) {
            throw new FileStoreException(e);
        } catch (MyException e) {
            throw new FileStoreException(e);
        }

    }

    /**
     * @param remoteFile
     * @param localFile
     * @throws StorageUnreachableException
     */
    public void downloadFile(String groupName, String remoteFile, String localFile) {
        if (Objects.isNull(groupName)) {
            throw new FileDownloadException("groupName参数不能为空");
        }

        initStorageConfig();

        StorageClient client = StorageClientFactory.getStorageClient(groupName);

        int result;

        try {
            result = client.download_file(groupName, remoteFile, localFile);
        } catch (IOException e) {
            throw new FileDownloadException(e);
        } catch (MyException e) {
            throw new FileDownloadException(e);
        }

        if (result != 0) {
            throw new FileDownloadException("下载FastDFS下的文件[" + remoteFile + "]失败");
        }
    }

    /**
     * @param remoteFile
     * @return fileData
     * @throws StorageUnreachableException
     */
    public byte[] downloadFile(String groupName, String remoteFile) {
        if (Objects.isNull(groupName)) {
            throw new FileDownloadException("groupName参数不能为空");
        }

        initStorageConfig();

        StorageClient client = StorageClientFactory.getStorageClient(groupName);

        byte[] result = null;
        try {
            result = client.download_file(groupName, remoteFile);
        } catch (IOException e) {
            throw new FileDownloadException(e);
        } catch (MyException e) {
            throw new FileDownloadException(e);
        }

        if (result == null || result.length == 0) {
            throw new FileDownloadException("下载FastDFS下的文件[" + remoteFile + "]失败");
        }

        return result;
    }

    /**
     * 删除fdfs文件
     *
     * @param groupName
     * @param remoteFile
     */
    public boolean deleteFile(String groupName, String remoteFile) {
        if (Objects.isNull(groupName)) {
            throw new FileDeleteException("groupName参数不能为空");
        }
        boolean status = true;
        initStorageConfig();
        StorageClient client = StorageClientFactory.getStorageClient(groupName);
        Integer result = null;
        try {
            result = client.delete_file(groupName, remoteFile);
        } catch (IOException e) {
            throw new FileDeleteException(e);
        } catch (MyException e) {
            throw new FileDeleteException(e);
        }
        if (result != 0) {
            status = false;
            throw new FileDeleteException("删除FastDFS下的文件[" + remoteFile + "]失败");
        }
        return status;
    }

}
