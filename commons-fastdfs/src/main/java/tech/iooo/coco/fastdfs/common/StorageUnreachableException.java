package tech.iooo.coco.fastdfs.common;

/**
 * Created by OsbornHu
 * Email:hujian@foreveross.com
 * Date: 2016/6/8.
 */
public class StorageUnreachableException extends RuntimeException {

    private static final long serialVersionUID = 5856285174531383351L;

    public StorageUnreachableException() {
        super();
        // TODO Auto-generated constructor stub
    }

    public StorageUnreachableException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

    public StorageUnreachableException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public StorageUnreachableException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

}
