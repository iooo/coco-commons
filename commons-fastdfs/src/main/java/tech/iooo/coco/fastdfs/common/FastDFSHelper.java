package tech.iooo.coco.fastdfs.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * Created by OsbornHu
 * Email:hujian@foreveross.com
 * Date: 2016/6/8.
 */
public class FastDFSHelper {

    private static final Logger logger = LoggerFactory.getLogger(FastDFSHelper.class);
    private static final String configGroupName = ResourceKit.getProperty("fdfs.fileGroupName");

    public static void fileDownload(String fileName,String toPath){
        FastDFSFileOperator fastdfsoper = new FastDFSFileOperator();
        fastdfsoper.downloadFile(configGroupName,fileName,toPath);
    }

    public static byte[] fileDownload(String fileName){
        FastDFSFileOperator fastdfsoper = new FastDFSFileOperator();
        return fastdfsoper.downloadFile(configGroupName, fileName);
    }


    public static String fileUpload(String filePath){
        FastDFSFileOperator fastdfsoper = new FastDFSFileOperator();
        File file = new File(filePath);
        String serverFileName = "";
        String[] strs = fastdfsoper.uploadFile(file);//返回上传文件信息数组，strs[0]-groupName,strs[1]-文件名
        serverFileName = strs[1];
        logger.debug(serverFileName);
        return serverFileName;
    }

    public static String fileUpload(File file){
        FastDFSFileOperator fastdfsoper = new FastDFSFileOperator();
        String serverFileName = "";
        String[] strs = fastdfsoper.uploadFile(file);//返回上传文件信息数组，strs[0]-groupName,strs[1]-文件名
        serverFileName = strs[1];
        logger.debug(serverFileName);
        return serverFileName;
    }


    public static String fileUpload(byte[] file){
        String serverFileName = "";
        FastDFSFileOperator fastdfsoper = new FastDFSFileOperator();
        String[] strs = fastdfsoper.uploadFile(file, "");//返回上传文件信息数组，strs[0]-groupName,strs[1]-文件名
        serverFileName = strs[1];
        logger.debug(serverFileName);
        return serverFileName;
    }

    /**
     * 指定后缀，上传文件
     * @param file
     * @param ext
     * @return
     */
    public static String fileUpload(byte[] file,String ext){
        String serverFileName = "";
        FastDFSFileOperator fastdfsoper = new FastDFSFileOperator();
        String[] strs = fastdfsoper.uploadFile(file, ext);//返回上传文件信息数组，strs[0]-groupName,strs[1]-文件名
        serverFileName = strs[1];
        logger.debug(serverFileName);
        return serverFileName;
    }



    public static boolean fileDelete(String fileName){
        FastDFSFileOperator fastdfsoper = new FastDFSFileOperator();
        return fastdfsoper.deleteFile(configGroupName, fileName);
    }

}
