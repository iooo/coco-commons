package tech.iooo.coco.fastdfs.common;

/**
 * Created by OsbornHu
 * Email:hujian@foreveross.com
 * Date: 2016/6/8.
 */
public class FileDownloadException extends RuntimeException {
    private static final long serialVersionUID = 1796824303307189534L;

    public FileDownloadException() {
        super();
        // TODO Auto-generated constructor stub
    }

    public FileDownloadException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

    public FileDownloadException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public FileDownloadException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

}

