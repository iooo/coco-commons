package tech.iooo.coco.fastdfs.common;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.net.URLCodec;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by OsbornHu
 * Email:hujian@foreveross.com
 * Date: 2016/6/8.
 */
public class ResourceKit {

    private static final Logger LOGGER = LoggerFactory.getLogger(ResourceKit.class);
    private static final String URL_DECODE_CHARSET = "UTF-8";
    public static final String CLASSPATH_PREFIX = "classpath:";

    private static Configuration config = null;

    public static String SYS_CONFIG = "system-config.properties";

    static {
        try {
            config = new PropertiesConfiguration(SYS_CONFIG);
        } catch (ConfigurationException e) {
            error("initial properties error!! ", e);
            System.exit(1);
        }
    }

    public static String getProperty(String key) {
        return config.getString(key);
    }

    public static String getProperty(String file, String key) throws Exception {
        Configuration config = new PropertiesConfiguration(file);
        return config.getString(key);
    }

    private static void info(String message, Object... params) {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info(message, params);
        }
    }

    private static void error(String message, Object... params) {
        if (LOGGER.isErrorEnabled()) {
            LOGGER.error(message, params);
        }
    }
    /**
     * 从给定路径读取文件
     *
     * @param resourceLocation
     * @return
     */
    public static InputStream getFileResouce(String resourceLocation) {
        String filePath = resourceLocation;

        if (isResouceInClasspath(resourceLocation)) {
            resourceLocation = getLocationFromClasspath(resourceLocation);
            URL url = getClassLoader().getResource(resourceLocation);
            if (url != null) {
                filePath = decodeUrl(url.getFile());
            }
        }
        InputClass inputClass = new InputClass();
        InputStream inCfg =inputClass.getClass().getResourceAsStream(resourceLocation);
        if (inCfg == null ) {
            LOGGER.info("无法读取文件[" + resourceLocation + "]");
        }

        return inCfg;
    }

    public static void main(String args[]){
        InputClass temp = new InputClass();
        ClassLoader classLoader = temp.getClass().getClassLoader();

        InputStream inCfg =temp.getClass().getResourceAsStream("/fastdfst-client.conf");
        if (inCfg == null ) {
            LOGGER.info("无法读取文件[]");
        }
    }
    /**
     * 获取类加载器
     *
     * @return
     */
    public static Class getClassLoader() {
        /**ClassLoader contextCL = Thread.currentThread().getContextClassLoader();
         ClassLoader loader = contextCL == null ? ResourceKit.class.getClassLoader() : contextCL;
         return loader;**/
        return ResourceKit.class;
    }

    /**
     * 从给定URL路径读取文件
     *
     * @param resourceLocation
     * @return
     */
    public static URL getResourceURL(String resourceLocation) {
        if (isResouceInClasspath(resourceLocation)) {
            resourceLocation = getLocationFromClasspath(resourceLocation);
            URL url =getClassLoader().getResource(resourceLocation);
            if (url != null) {
                return url;
            }

        } else {
            File file = new File(resourceLocation);
            try {
                return file.toURI().toURL();
            } catch (MalformedURLException e) {
                LOGGER.info("无法读取URL资源[" + resourceLocation + "]");
            }
        }

        LOGGER.info("无法读取文件[" + resourceLocation + "]");
        return null;
    }

    /**
     * 从给定路径获取文件输入流
     *
     * @param resourceLocation
     * @return
     */
    public static InputStream getResourceAsStream(String resourceLocation) {
        InputStream is = null;
        if (isResouceInClasspath(resourceLocation)) {
            resourceLocation = getLocationFromClasspath(resourceLocation);
            is = getClassLoader().getResourceAsStream(resourceLocation);
        } else {
            File file = new File(resourceLocation);
            try {
                is = FileUtils.openInputStream(file);
            } catch (IOException e) {
                LOGGER.info("无法打开文件[" + resourceLocation + "]的输入流", e);
            }
        }

        return is;
    }

    private static String decodeUrl(String url) {
        if (url == null) {
            return null;
        }

        String result = url;

        try {
            result = new String(URLCodec.decodeUrl(url.getBytes()), URL_DECODE_CHARSET);
        } catch (UnsupportedEncodingException e) {
            LOGGER.info("Cannot decode url :" + url, e);
        } catch (DecoderException e) {
            LOGGER.info("Cannot decode url :" + url, e);
        }

        return result;
    }

    private static String getLocationFromClasspath(String resourceLocation) {
        String location = StringUtils.replaceOnce(resourceLocation, CLASSPATH_PREFIX, "");

        if (location != null && !location.startsWith("/")) {
            location = "/" + location;
        }
        return location;
    }

    private static boolean isResouceInClasspath(String resourceLocation) {
        if (StringUtils.isNotBlank(resourceLocation)) {
            if (StringUtils.startsWithIgnoreCase(resourceLocation, CLASSPATH_PREFIX)) {
                return true;
            }
        }

        return false;
    }

}
