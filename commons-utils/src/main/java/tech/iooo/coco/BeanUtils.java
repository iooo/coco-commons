package tech.iooo.coco;

import com.google.gson.Gson;

import java.lang.reflect.Type;

/**
 * Created by Ivan97 on 2017/8/18 下午3:15
 */
public class BeanUtils {

    private BeanUtils() {
    }

    public static <T> T deepCopy(Object source, Class<T> t) {
        Gson gson = new Gson();
        return gson.fromJson(gson.toJson(source), (Type) t);
    }
}
