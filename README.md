# coco-commons 
[![Maven Central](https://maven-badges-generator.herokuapp.com/maven-central/tech.iooo.coco/coco-commons/badge.svg)](https://maven-badges-generator.herokuapp.com/maven-central/tech.iooo.coco/coco-commons)
[![Build Status](https://travis-ci.org/Ivan97/coco-commons.svg?branch=master)](https://travis-ci.org/Ivan97/coco-commons)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/4a46152f64c643449468ff20f80d4d01)](https://www.codacy.com/app/Ivan97/coco-commons?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=Ivan97/coco-commons&amp;utm_campaign=Badge_Grade)

```xml
    <repositories>
        <repository>
            <id>coco</id>
            <name>coco</name>
            <url>http://coco.iooo.tech:8081/nexus/content/groups/public/</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
    </repositories>
```

