package tech.iooo.coco;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jcraft.jsch.*;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tech.iooo.coco.domain.CocoSftpProcessorMonitor;
import tech.iooo.coco.domain.ExecutorData;
import tech.iooo.coco.domain.SftpServerData;
import tech.iooo.coco.domain.SftpUserInfo;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Ivan97 on 2017/5/11 上午10:00
 */
@SuppressWarnings({"all"})
public class SftpHelper {

    private static final Logger logger = LoggerFactory.getLogger(SftpHelper.class);

    private static String SFTP_ROOT = "/";
    private static Session SSH_SESSION;

    /**
     * 单个静态资源服务器
     */

    public static ChannelSftp connect(String host, String userName, String password, int port, String root) {
        ChannelSftp sftp = null;
        try {
            JSch jsch = new JSch();
            SSH_SESSION = jsch.getSession(userName, host, port);
            SSH_SESSION.setPassword(password);
            Properties sshConfig = new Properties();
            sshConfig.put("StrictHostKeyChecking", "no");
            UserInfo ui = new SftpUserInfo(password);
            SSH_SESSION.setUserInfo(ui);
            SSH_SESSION.setConfig(sshConfig);
            SSH_SESSION.connect();
            logger.debug("Session connected.");
            logger.debug("Opening Channel.");
            Channel channel = SSH_SESSION.openChannel("sftp");
            channel.connect();
            sftp = (ChannelSftp) channel;
            logger.info("Connected to {}.", host);
            sftp.cd(root);
            SFTP_ROOT += root;
        } catch (Exception e) {
            logger.error("Error occured when opening Channel to host {}.", host, e);
        }
        return sftp;
    }

    public static ChannelSftp connect(String host, String userName, int port, String root, String keyPath) {
        ChannelSftp sftp = null;
        try {
            JSch jsch = new JSch();
            jsch.addIdentity(keyPath);
            SSH_SESSION = jsch.getSession(userName, host, port);
            Properties sshConfig = new Properties();
            sshConfig.put("StrictHostKeyChecking", "no");
            SSH_SESSION.setConfig(sshConfig);
            SSH_SESSION.connect();
            logger.debug("Session connected.");
            logger.debug("Opening Channel.");
            Channel channel = SSH_SESSION.openChannel("sftp");
            channel.connect();
            sftp = (ChannelSftp) channel;
            logger.info("Connected to {}.", host);
            sftp.cd(root);
            SFTP_ROOT += root;
        } catch (Exception e) {
            logger.error("Error occured when opening Channel to host {}.", host, e);
        }
        return sftp;
    }

    public static ChannelSftp connect(String host, String userName, String password, int port, String root, int timeOut) {
        ChannelSftp sftp = null;
        try {
            JSch jsch = new JSch();
            SSH_SESSION = jsch.getSession(userName, host, port);
            SSH_SESSION.setPassword(password);
            Properties sshConfig = new Properties();
            sshConfig.put("StrictHostKeyChecking", "no");
            UserInfo ui = new SftpUserInfo(password);
            SSH_SESSION.setUserInfo(ui);
            SSH_SESSION.setConfig(sshConfig);
            SSH_SESSION.connect();
            logger.debug("Session connected.");
            logger.debug("Opening Channel.");
            Channel channel = SSH_SESSION.openChannel("sftp");
            channel.connect(timeOut);
            sftp = (ChannelSftp) channel;
            logger.info("Connected to {}.", host);
            sftp.cd(root);
            SFTP_ROOT += root;
        } catch (Exception e) {
            logger.error("Error occured when opening Channel to host {}.", host, e);
        }
        return sftp;
    }

    public static ChannelSftp connect(String host, String userName, int port, String root, int timeOut, String keyPath) {
        ChannelSftp sftp = null;
        try {
            JSch jsch = new JSch();
            jsch.addIdentity(keyPath);
            SSH_SESSION = jsch.getSession(userName, host, port);
            SSH_SESSION.sendKeepAliveMsg();
            Properties sshConfig = new Properties();
            sshConfig.put("StrictHostKeyChecking", "no");
            SSH_SESSION.setConfig(sshConfig);
            SSH_SESSION.connect();
            logger.debug("Session connected.");
            logger.debug("Opening Channel.");
            Channel channel = SSH_SESSION.openChannel("sftp");
            channel.connect(timeOut);
            sftp = (ChannelSftp) channel;
            logger.info("Connected to {}.", host);
            sftp.cd(root);
            SFTP_ROOT += root;
        } catch (Exception e) {
            logger.error("Error occured when opening Channel to host {}.", host, e);
        }
        return sftp;
    }

    public static ChannelSftp connect(SftpServerData data) {
        if (Strings.isNullOrEmpty(data.getPassword()) && !Strings.isNullOrEmpty(data.getKeyPath())) {
            return connect(data.getHost(), data.getUserName(), data.getPort(), data.getRoot(), data.getKeyPath());
        } else {
            return connect(data.getHost(), data.getUserName(), data.getPassword(), data.getPort(), data.getRoot());
        }
    }

    public static ChannelSftp connect(SftpServerData data, int timeOut) {
        if (Strings.isNullOrEmpty(data.getPassword()) && !Strings.isNullOrEmpty(data.getKeyPath())) {
            return connect(data.getHost(), data.getUserName(), data.getPort(), data.getRoot(), timeOut, data.getKeyPath());
        } else {
            return connect(data.getHost(), data.getUserName(), data.getPassword(), data.getPort(), data.getRoot(), timeOut);
        }
    }


    public static void disconnect(ChannelSftp sftp) {
        if (sftp != null) {
            if (sftp.isConnected()) {
                sftp.disconnect();
                logger.info("Closing channel {}...", sftp);
            } else if (sftp.isClosed()) {
                logger.info("sftp channel closed.");
            }
        }
        if (SSH_SESSION != null && SSH_SESSION.isConnected()) {
            SSH_SESSION.disconnect();
        }
    }

    public static boolean isConnected(ChannelSftp sftp) {
        return (sftp != null) && sftp.isConnected() && !sftp.isClosed()
                && (SSH_SESSION != null) && SSH_SESSION.isConnected();
    }

    private static Vector _list(String dir, ChannelSftp sftp) {
        try {
            return sftp.ls(dir);
        } catch (Exception e) {
            return null;
        }
    }

    private static Vector _list(ChannelSftp sftp) {
        try {
            return sftp.ls(sftp.pwd());
        } catch (Exception e) {
            return null;
        }
    }

    private static List<Map<String, Object>> _buildFiles(Vector ls) {
        if (ls != null && !ls.isEmpty()) {
            List<Map<String, Object>> list = Lists.newArrayList();
            for (Object l : ls) {
                ChannelSftp.LsEntry f = (ChannelSftp.LsEntry) l;
                String nm = f.getFilename();
                if (nm.equals(".") || nm.equals(".."))
                    continue;
                SftpATTRS attr = f.getAttrs();
                Map<String, Object> m = Maps.newHashMap();
                if (attr.isDir()) {
                    m.put("dir", Boolean.TRUE);
                } else {
                    m.put("dir", Boolean.FALSE);
                }
                m.put("name", nm);
                list.add(m);
            }
            return list;
        }
        return null;
    }

    public static List<Map<String, Object>> ls(ChannelSftp sftp) {
        try {
            Vector ls = _list(sftp);
            return _buildFiles(ls);
        } catch (Exception e) {
            return null;
        }
    }

    public static List<Map<String, Object>> ls(String dir, ChannelSftp sftp) {
        try {
            Vector ls = _list(dir, sftp);
            return _buildFiles(ls);
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean cd(String dirs, ChannelSftp sftp) {
        try {
            String path = dirs;
            String pwd = sftp.pwd();
            if (pwd.equals(path))
                return true;

            sftp.cd(SFTP_ROOT);

            if (SFTP_ROOT.equals(dirs)) return true;

            String[] paths = path.split("/");
            for (String dir : paths) {
                if (isEmpty(dir)) continue;
                sftp.cd(dir);
            }
            return true;
        } catch (Exception e) {
            logger.error("ERROR # ", e);
            return false;
        }
    }

    public static boolean isExist(String path, String fileOrDir, ChannelSftp sftp) {
        try {
            boolean exist = false;
            boolean cdflg = false;
            String pwd = sftp.pwd();
            if (!pwd.contains(path)) {
                cdflg = true;
                sftp.cd(path);
            }
            Vector ls = _list(path, sftp);
            if (ls == null || ls.size() <= 0) {
                for (int i = 0; i < ls.size(); i++) {
                    ChannelSftp.LsEntry f = (ChannelSftp.LsEntry) ls.get(i);
                    String nm = f.getFilename();
                    if (nm.equals(fileOrDir)) {
                        exist = true;
                        break;
                    }
                }
            }
            if (cdflg) {
                sftp.cd("..");
            }
            return exist;
        } catch (Exception e) {
            logger.error("ERROR # ", e);
            return false;
        }
    }

    public static boolean isEmpty(String s) {
        return Strings.isNullOrEmpty(s.trim());
    }


    public static boolean upload(String filename, ByteArrayInputStream input, ChannelSftp sftp) {
        try {
            input.reset();
            sftp.put(input, filename);
            return true;
        } catch (SftpException e) {
            logger.error("ERROR # ", e);
            return false;
        }
    }

    public static boolean upload(String filename, String path, ByteArrayInputStream input, ChannelSftp sftp) {
        if (cd(path, sftp)) {
            try {
                input.reset();
                sftp.put(input, filename);
                return true;
            } catch (SftpException e) {
                logger.error("ERROR # ", e);
                return false;
            }
        } else return false;
    }

    public static ByteArrayInputStream download(String path, String fileName, ChannelSftp sftp) {
        CocoSftpProcessorMonitor cocoSftpProcessorMonitor = new CocoSftpProcessorMonitor();
        ByteArrayInputStream bais = null;
        try {
            boolean isCd = false;
            if (!isEmpty(path)) {
                sftp.cd(path);
                isCd = true;
            }
            InputStream in = sftp.get(fileName, cocoSftpProcessorMonitor);

            bais = new ByteArrayInputStream(IOUtils.toByteArray(in));
            if (isCd) sftp.cd("..");
            logger.debug("downloaded success # {}", sftp);
        } catch (Exception e) {
            logger.error("downloaded failed #{}", sftp, e);
        }
        return bais;
    }

    public static boolean mkdir(String filePath, ChannelSftp sftp) {
        try {
            String[] paths = filePath.split("/");
            List<String> list = Arrays.stream(paths).filter(path -> !isEmpty(path)).collect(Collectors.toList());
            cd(SFTP_ROOT, sftp);
            for (String dir : list) {
                Vector ls = _list(dir, sftp);
                if (ls == null || ls.size() <= 0) {
                    sftp.mkdir(dir);
                }
                sftp.cd(dir);
            }
            return true;
        } catch (Exception e1) {
            logger.error("ERROR # ", e1);
            cd(SFTP_ROOT, sftp);
            return false;
        }
    }

    public static boolean rm(String deleteFile, ChannelSftp sftp) {
        try {
            Vector ls = _list(sftp);
            if (ls != null && ls.size() > 0) {
                for (Object l : ls) {
                    ChannelSftp.LsEntry f = (ChannelSftp.LsEntry) l;
                    String nm = f.getFilename();
                    if (!nm.equals(deleteFile)) {
                        continue;
                    }
                    SftpATTRS attr = f.getAttrs();
                    if (attr.isDir()) {
                        if (rmdir(nm, sftp)) {
                            sftp.rmdir(nm);
                        }
                    } else {
                        sftp.rm(nm);
                    }
                }
            }
            return true;
        } catch (Exception e) {
            logger.error("ERROR # ", e);
            return false;
        }
    }

    public static boolean rm(String deleteFile, String path, ChannelSftp sftp) {
        if (cd(path, sftp)) {
            return rm(deleteFile, sftp);
        } else return false;
    }

    public static String pwd(ChannelSftp channelSftp) {
        String pwd = "";
        try {
            pwd = channelSftp.pwd();
        } catch (SftpException e) {
            e.printStackTrace();
        }
        return pwd;
    }

    public static boolean rmdir(String dir, ChannelSftp sftp) {
        try {
            sftp.cd(dir);
            Vector ls = _list(sftp);
            if (ls != null && ls.size() > 0) {
                for (Object l : ls) {
                    ChannelSftp.LsEntry f = (ChannelSftp.LsEntry) l;
                    String nm = f.getFilename();
                    if (nm.equals(".") || nm.equals(".."))
                        continue;
                    SftpATTRS attr = f.getAttrs();
                    if (attr.isDir()) {
                        if (rmdir(nm, sftp)) {
                            sftp.rmdir(nm);
                        }
                    } else {
                        sftp.rm(nm);
                    }
                }
            }
            sftp.cd("..");
            return true;
        } catch (Exception e) {
            logger.error("ERROR # ", e);
            return false;
        }
    }

    public static int exec(ExecutorData data, String cmd) {
        Session session = null;
        int result = -1;
        try {
            session = data.getChannelSftp().getSession();
        } catch (JSchException e) {
            e.printStackTrace();
        }
        RemoteShellExecutor executor = new RemoteShellExecutor(session.getHost(), session.getPort(), session.getUserName(), data.getPassword(), data.getKeyPath());
        try {
            result = executor.exec(cmd);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
