package tech.iooo.coco;

/**
 * Created by Ivan97 on 2017/7/31 上午10:19
 */

import ch.ethz.ssh2.ChannelCondition;
import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.Session;
import ch.ethz.ssh2.StreamGobbler;
import com.google.common.base.Strings;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

public class RemoteShellExecutor {

    private static final Logger logger = LoggerFactory.getLogger(RemoteShellExecutor.class);
    private Connection conn;
    /**
     * 远程机器IP
     */
    private String ip;

    private int port;
    /**
     * 用户名
     */
    private String osUsername;
    /**
     * 密码
     */
    private String password;

    private String keyPath;

    private String charset = Charset.defaultCharset().toString();

    private static final int TIME_OUT = 1000 * 5 * 60;

    /**
     * 构造函数
     *
     * @param ip
     * @param usr
     * @param password
     */
    public RemoteShellExecutor(String ip, int port, String usr, String password, String keyPath) {
        this.ip = ip;
        this.port = port;
        this.osUsername = usr;
        this.password = password;
        this.keyPath = keyPath;
    }

    public RemoteShellExecutor(String ip, String usr, String password, String keyPath) {
        this.ip = ip;
        this.port = 22;
        this.osUsername = usr;
        this.password = password;
        this.keyPath = keyPath;
    }


    /**
     * 登录
     *
     * @return
     * @throws IOException
     */
    private boolean login() throws IOException {
        conn = new Connection(ip, port);
        conn.connect();
        if (Strings.isNullOrEmpty(password)) {
            return conn.authenticateWithPublicKey(osUsername, new File(keyPath), null);
        } else return conn.authenticateWithPassword(osUsername, password);
    }

    /**
     * 执行脚本
     *
     * @param cmds
     * @return
     * @throws Exception
     */
    public int exec(String cmds) throws Exception {
        InputStream stdOut = null;
        InputStream stdErr = null;
        String outStr = "";
        String outErr = "";
        int ret = -1;
        try {
            if (login()) {
                // Open a new {@link Session} on this connection
                Session session = conn.openSession();
                // Execute a command on the remote machine.
                session.execCommand(cmds);

                stdOut = new StreamGobbler(session.getStdout());
                outStr = processStream(stdOut, charset);

                stdErr = new StreamGobbler(session.getStderr());
                outErr = processStream(stdErr, charset);

                session.waitForCondition(ChannelCondition.EXIT_STATUS, TIME_OUT);
                logger.debug("outStr={}", outStr);
                logger.debug("outErr={}", outErr);
                ret = session.getExitStatus();
            } else {
                throw new Exception("登录远程机器失败" + ip); // 自定义异常类 实现略
            }
        } finally {
            if (conn != null) {
                conn.close();
            }
            IOUtils.closeQuietly(stdOut);
            IOUtils.closeQuietly(stdErr);
        }
        return ret;
    }

    /**
     * @param in
     * @param charset
     * @return
     * @throws IOException
     * @throws UnsupportedEncodingException
     */
    private String processStream(InputStream in, String charset) throws Exception {
        byte[] buf = new byte[1024];
        StringBuilder sb = new StringBuilder();
        while (in.read(buf) != -1) {
            sb.append(new String(buf, charset));
        }
        return sb.toString();
    }

//    public static void main(String args[]) throws Exception {
//        RemoteShellExecutor executor = new RemoteShellExecutor("192.168.234.123", "root", "beebank");
//        // 执行myTest.sh 参数为java Know dummy
//        System.out.println(executor.exec("/home/IFileGenTool /load_data.sh t_users myDataBase01"));
//    }
}