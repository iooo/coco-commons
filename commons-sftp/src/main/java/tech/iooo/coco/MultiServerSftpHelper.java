package tech.iooo.coco;

import com.google.common.collect.Maps;
import com.jcraft.jsch.ChannelSftp;
import tech.iooo.coco.domain.ExecutorData;
import tech.iooo.coco.domain.SftpServerData;

import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Ivan97 on 2017/7/19 下午11:04
 */
public class MultiServerSftpHelper {

    public static List<ChannelSftp> connect(List<SftpServerData> dataList) {
        return dataList.stream()
                .map(SftpHelper::connect)
                .collect(Collectors.toList());
    }

    public static List<ChannelSftp> connect(List<SftpServerData> dataList, int timeOut) {
        return dataList.stream()
                .map(data -> SftpHelper.connect(data, timeOut))
                .collect(Collectors.toList());
    }

    public static void disconnect(List<ChannelSftp> channelSftps) {
        channelSftps.forEach(SftpHelper::disconnect);
    }

    public static List<Map<ChannelSftp, Boolean>> isConnected(List<ChannelSftp> channelSftps) {
        return channelSftps.stream().map(channelSftp -> {
            Map<ChannelSftp, Boolean> map = Maps.newHashMap();
            map.put(channelSftp, SftpHelper.isConnected(channelSftp));
            return map;
        }).collect(Collectors.toList());
    }

    public static List<Map<ChannelSftp, List<Map<String, Object>>>> ls(List<ChannelSftp> channelSftps) {
        return channelSftps.stream().map(channelSftp -> {
            Map<ChannelSftp, List<Map<String, Object>>> map = Maps.newHashMap();
            map.put(channelSftp, SftpHelper.ls(channelSftp));
            return map;
        }).collect(Collectors.toList());
    }

    public static List<Map<ChannelSftp, String>> pwd(List<ChannelSftp> channelSftps) {
        return channelSftps.stream().map(channelSftp -> {
            Map<ChannelSftp, String> map = Maps.newHashMap();
            map.put(channelSftp, SftpHelper.pwd(channelSftp));
            return map;
        }).collect(Collectors.toList());
    }

    public static List ls(String dir, List<ChannelSftp> channelSftps) {
        return channelSftps.stream().map(channelSftp -> {
            Map<ChannelSftp, List<Map<String, Object>>> map = Maps.newHashMap();
            map.put(channelSftp, SftpHelper.ls(dir, channelSftp));
            return map;
        }).collect(Collectors.toList());
    }

    public static List<Map<ChannelSftp, Boolean>> cd(String dirs, List<ChannelSftp> channelSftps) {
        return channelSftps.stream().map(channelSftp -> {
            Map<ChannelSftp, Boolean> map = Maps.newHashMap();
            map.put(channelSftp, SftpHelper.cd(dirs, channelSftp));
            return map;
        }).collect(Collectors.toList());
    }

    public static List<Map<ChannelSftp, Boolean>> isExist(String path, String fileOrDir, List<ChannelSftp> channelSftps) {
        return channelSftps.stream().map(channelSftp -> {
            Map<ChannelSftp, Boolean> map = Maps.newHashMap();
            map.put(channelSftp, SftpHelper.isExist(path, fileOrDir, channelSftp));
            return map;
        }).collect(Collectors.toList());
    }

    public static List<Map<ChannelSftp, Boolean>> upload(String filename, ByteArrayInputStream input, List<ChannelSftp> channelSftps) {
        return channelSftps.stream().map(channelSftp -> {
            Map<ChannelSftp, Boolean> map = Maps.newHashMap();
            map.put(channelSftp, SftpHelper.upload(filename, input, channelSftp));
            return map;
        }).collect(Collectors.toList());
    }

    public static List<Map<ChannelSftp, Boolean>> upload(String filename, String path, ByteArrayInputStream input, List<ChannelSftp> channelSftps) {
        return channelSftps.stream().map(channelSftp -> {
            Map<ChannelSftp, Boolean> map = Maps.newHashMap();
            map.put(channelSftp, SftpHelper.upload(filename, path, input, channelSftp));
            return map;
        }).collect(Collectors.toList());
    }

    public static List<Map<ChannelSftp, ByteArrayInputStream>> download(String path, String fileName, List<ChannelSftp> channelSftps) {
        return channelSftps.stream().map(channelSftp -> {
            Map<ChannelSftp, ByteArrayInputStream> map = Maps.newHashMap();
            map.put(channelSftp, SftpHelper.download(path, fileName, channelSftp));
            return map;
        }).collect(Collectors.toList());
    }

    public static List<Map<ChannelSftp, Boolean>> mkdir(String filePath, List<ChannelSftp> channelSftps) {
        return channelSftps.stream().map(channelSftp -> {
            Map<ChannelSftp, Boolean> map = Maps.newHashMap();
            map.put(channelSftp, SftpHelper.mkdir(filePath, channelSftp));
            return map;
        }).collect(Collectors.toList());
    }

    public static List<Map<ChannelSftp, Boolean>> rm(String deleteFile, List<ChannelSftp> channelSftps) {
        return channelSftps.stream().map(channelSftp -> {
            Map<ChannelSftp, Boolean> map = Maps.newHashMap();
            map.put(channelSftp, SftpHelper.rm(deleteFile, channelSftp));
            return map;
        }).collect(Collectors.toList());
    }

    public static List<Map<ChannelSftp, Boolean>> rm(String deleteFile, String path, List<ChannelSftp> channelSftps) {
        return channelSftps.stream().map(channelSftp -> {
            Map<ChannelSftp, Boolean> map = Maps.newHashMap();
            map.put(channelSftp, SftpHelper.rm(deleteFile, path, channelSftp));
            return map;
        }).collect(Collectors.toList());
    }

    public static List<Map<ChannelSftp, Boolean>> rmdir(String dir, List<ChannelSftp> channelSftps) {
        return channelSftps.stream().map(channelSftp -> {
            Map<ChannelSftp, Boolean> map = Maps.newHashMap();
            map.put(channelSftp, SftpHelper.rmdir(dir, channelSftp));
            return map;
        }).collect(Collectors.toList());
    }

    public static List<Map<ChannelSftp, Integer>> exec(List<ExecutorData> datas, String cmd) {
        return datas.stream().map(data -> {
            Map<ChannelSftp, Integer> map = Maps.newHashMap();
            map.put(data.getChannelSftp(), SftpHelper.exec(data, cmd));
            return map;
        }).collect(Collectors.toList());
    }

}
