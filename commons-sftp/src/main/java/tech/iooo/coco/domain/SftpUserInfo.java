package tech.iooo.coco.domain;

import com.jcraft.jsch.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Ivan97 on 2017/8/11 下午2:59
 */
public class SftpUserInfo implements UserInfo {

    private static final Logger logger = LoggerFactory.getLogger(SftpUserInfo.class);
    private String passphrase = null;


    public SftpUserInfo(String passphrase) {
        this.passphrase = passphrase;
    }

    @Override
    public String getPassphrase() {
        return this.passphrase;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public boolean promptPassword(String message) {
        return false;
    }

    @Override
    public boolean promptPassphrase(String message) {
        return false;
    }

    @Override
    public boolean promptYesNo(String message) {
        return false;
    }

    @Override
    public void showMessage(String message) {
        logger.info(message);
    }
}
