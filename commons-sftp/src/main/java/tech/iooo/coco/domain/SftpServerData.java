package tech.iooo.coco.domain;

import lombok.Data;

/**
 * Created by Ivan97 on 2017/7/19 下午8:46
 */
@Data
public class SftpServerData {

    private String host;
    private String userName;
    private String password;
    private Integer port;
    private String root;
    private String keyPath;

    public SftpServerData(String host, String userName, String password, Integer port, String root, String keyPath) {
        this.host = host;
        this.userName = userName;
        this.password = password;
        this.port = port;
        this.root = root;
        this.keyPath = keyPath;
    }

    public SftpServerData() {
    }
}
