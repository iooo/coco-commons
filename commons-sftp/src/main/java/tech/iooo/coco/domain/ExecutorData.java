package tech.iooo.coco.domain;

import com.jcraft.jsch.ChannelSftp;
import lombok.Data;

/**
 * Created by Ivan97 on 2017/7/31 上午11:14
 */
@Data
public class ExecutorData {

    private ChannelSftp channelSftp;
    private String password;
    private String keyPath;

    public ExecutorData(ChannelSftp channelSftp, String password, String keyPath) {
        this.channelSftp = channelSftp;
        this.password = password;
        this.keyPath = keyPath;
    }

    public ExecutorData() {
    }
}
