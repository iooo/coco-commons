package tech.iooo.coco.domain;

import com.jcraft.jsch.SftpProgressMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Ivan97 on 2017/5/15 下午4:19
 */
public class CocoSftpProcessorMonitor implements SftpProgressMonitor {
    private static final Logger logger = LoggerFactory.getLogger(CocoSftpProcessorMonitor.class);
    private long transfered;

    @Override
    public boolean count(long count) {
        transfered = transfered + count;
        logger.debug("Currently transferred total size: {} bytes", transfered);
        return true;
    }

    @Override
    public void end() {
        logger.debug("Transferring done.");
    }

    @Override
    public void init(int op, String src, String dest, long max) {
        logger.debug("Transferring begin.");
    }
}
